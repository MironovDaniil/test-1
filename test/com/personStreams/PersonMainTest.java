package com.personStreams;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PersonMainTest {
    private List<Person> people;

    @Before
    public void setUp() {
        people = new ArrayList<>();
        Person person1 = mock(Person.class);
        when(person1.getFullAge()).thenReturn(20);
        Person person2 = mock(Person.class);
        when(person2.getFullAge()).thenReturn(34);
        Person person3 = mock(Person.class);
        when(person3.getFullAge()).thenReturn(18);
        Person person4 = mock(Person.class);
        when(person4.getFullAge()).thenReturn(20);
        Person person5 = mock(Person.class);
        when(person5.getFullAge()).thenReturn(25);
        Person person6 = mock(Person.class);
        when(person6.getFullAge()).thenReturn(34);
        Person person7 = mock(Person.class);
        when(person7.getFullAge()).thenReturn(23);
        Person person8 = mock(Person.class);
        when(person8.getFullAge()).thenReturn(25);
        Person person9 = mock(Person.class);
        when(person9.getFullAge()).thenReturn(19);
        Person person10 = mock(Person.class);
        when(person10.getFullAge()).thenReturn(22);
        Person person11 = mock(Person.class);
        when(person11.getFullAge()).thenReturn(34);
        people.add(person1);
        people.add(person2);
        people.add(person3);
        people.add(person4);
        people.add(person5);
        people.add(person6);
        people.add(person7);
        people.add(person8);
        people.add(person9);
        people.add(person10);
        people.add(person11);
    }

    @Test
    public void clearFifth_removesTwoPersons() {
        Person remove1 = people.get(4);
        Person remove2 = people.get(9);
        List<Person> expected = new ArrayList<>(people.size());
        expected.addAll(people);
        expected.remove(remove1);
        expected.remove(remove2);
        List<Person> actual = PersonMain.clearFifth(people);
        assertEquals(expected, actual);
    }

    @Test
    public void ageCount_countsAges() {
        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(18, 1);
        expected.put(19, 1);
        expected.put(20, 2);
        expected.put(22, 1);
        expected.put(23, 1);
        expected.put(25, 2);
        expected.put(34, 3);
        Map<Integer, Integer> actual = PersonMain.ageCount(people);
        assertEquals(expected, actual);
    }
}
