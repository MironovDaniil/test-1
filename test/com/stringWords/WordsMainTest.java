package com.stringWords;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.HashMap;
import java.util.Map;

public class WordsMainTest {

    private String sentence1, sentence2;

    @Before
    public void setUp() {
        sentence1 = "Sentence to test without punctuation";
        sentence2 = "Test sentence, that test's needed method with punctuation.";
    }

    @Test
    public void wordPerLengthCounter_sentence1_countsWords() {
        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(2, 1);
        expected.put(4, 1);
        expected.put(8, 1);
        expected.put(7, 1);
        expected.put(11, 1);
        Map<Integer, Integer> actual = WordsMain.wordPerLengthCounter(sentence1);
        assertEquals(expected, actual);
    }

    @Test
    public void wordPerLengthCounter_sentence2_countsWords() {
        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(4, 3);
        expected.put(5, 1);
        expected.put(8, 1);
        expected.put(6, 2);
        expected.put(11, 1);
        Map<Integer, Integer> actual = WordsMain.wordPerLengthCounter(sentence2);
        assertEquals(expected, actual);
    }

    @Test
    public void wordPerLengthCounter_sentence2_countsWords_shouldFail() {
        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(4, 3);
        expected.put(9, 1);
        expected.put(6, 3);
        expected.put(12, 1);
        Map<Integer, Integer> actual = WordsMain.wordPerLengthCounter(sentence2);
        assertNotEquals(expected, actual);
    }


}
