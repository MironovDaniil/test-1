package com.binaryTree;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Comparator;

public class TreeNodeTest {
    private TreeNode<Object> root;

    @Before
    public void setUp() {
        Comparator comparator = Mockito.mock(Comparator.class);
        Object object1 = new Object();
        Object object2 = new Object();
        Object object3 = new Object();
        Object object4 = new Object();
        Object object5 = new Object();
        when(comparator.compare(object1,object1)).thenReturn(0);
        when(comparator.compare(object1,object2)).thenReturn(-1);
        when(comparator.compare(object1,object3)).thenReturn(-2);
        when(comparator.compare(object1,object4)).thenReturn(-3);
        when(comparator.compare(object1,object5)).thenReturn(-4);
        when(comparator.compare(object2,object1)).thenReturn(1);
        when(comparator.compare(object2,object2)).thenReturn(0);
        when(comparator.compare(object2,object3)).thenReturn(-1);
        when(comparator.compare(object2,object4)).thenReturn(-2);
        when(comparator.compare(object2,object5)).thenReturn(-3);
        when(comparator.compare(object3,object1)).thenReturn(2);
        when(comparator.compare(object3,object2)).thenReturn(1);
        when(comparator.compare(object3,object3)).thenReturn(0);
        when(comparator.compare(object3,object4)).thenReturn(-1);
        when(comparator.compare(object3,object5)).thenReturn(-2);
        when(comparator.compare(object4,object1)).thenReturn(3);
        when(comparator.compare(object4,object2)).thenReturn(2);
        when(comparator.compare(object4,object3)).thenReturn(1);
        when(comparator.compare(object4,object4)).thenReturn(0);
        when(comparator.compare(object4,object5)).thenReturn(-1);
        when(comparator.compare(object5,object1)).thenReturn(4);
        when(comparator.compare(object5,object2)).thenReturn(3);
        when(comparator.compare(object5,object3)).thenReturn(2);
        when(comparator.compare(object5,object4)).thenReturn(1);
        when(comparator.compare(object5,object5)).thenReturn(0);
        root = new TreeNode<>(object4,comparator);
        root.addValue(object2);
        root.addValue(object1);
        root.addValue(object3);
        root.addValue(object5);
    }

    @Test
    public void addValue_add6() {
        Object object1 = root.getLeft().getLeft().getValue();
        Object object2 = root.getLeft().getRight().getValue();
        Object object3 = root.getLeft().getValue();
        Object object6 = new Object();
        Object object4 = root.getValue();
        Object object5 = root.getRight().getValue();
        Comparator comparator = root.getComparator();
        when(comparator.compare(object6,object4)).thenReturn(2);
        when(comparator.compare(object6,object5)).thenReturn(1);
        when(comparator.compare(object5,object6)).thenReturn(-1);
        when(comparator.compare(object4,object6)).thenReturn(-2);
        when(comparator.compare(object6,object6)).thenReturn(0);
        root.addValue(object6);
        assertEquals(object6,root.getRight().getRight().getValue());
    }

    @Test
    public void treeHeight_countsTreeHeight_shouldBe3 () {
        assertEquals(3,TreeNode.treeHeight(root));
    }
}
