package com.CompanyStructure;

import com.companyStructure.Company;
import com.companyStructure.Employee;
import com.companyStructure.Manager;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CompanyTest {
    private Company company;

    @Before
    public void setUp() {
        company = new Company("John Smith");
        Employee maxPayne = new Employee(null, 1, true, "Max Payne");
        Employee martinScavo = new Employee(null, 2, false, "Martin Scavo");
        Employee gregHouse = new Employee(null, 3, false, "Greg House");
        List<Employee> brads = new ArrayList<>(2);
        brads.add(maxPayne);
        brads.add(martinScavo);
        List<Employee> jacks = new ArrayList<>(1);
        jacks.add(gregHouse);
        Employee bradScott = new Manager(null, 3, false, brads, "Brad Scott");
        List<Employee> freds = new ArrayList<>(1);
        freds.add(bradScott);
        Employee fredJackson = new Manager(company.getBigBoss(), 9, true, freds, "Fred Jackson");
        Employee jackOlivo = new Manager(company.getBigBoss(), 8, false, jacks, "Jack Olivo");
        Employee derickRed = new Employee(company.getBigBoss(), 4, true, "Derick Red");
    }

    @Test
    public void getByName_getsGreg() {
        String search = "Greg House";
        Employee actual = company.getByName(search);
        assertEquals(search, actual.getName());
    }

    @Test
    public void deleteEmployee_deleteBrad() {
        Employee brad = company.getByName("Brad Scott");
        Employee fred = brad.getBoss();
        company.deleteEmployee(brad);
        String expected = "Max Payne";
        String actual = fred.getDirectReports().get(0).getName();
        assertEquals(expected, actual);
    }

    @Test
    public void promotePayGrade_promoteDerick() {
        Employee derick = company.getByName("Derick Red");
        company.promotePayGrade(derick);
        assertEquals(5, derick.getPayGrade());
    }

    @Test
    public void promotePayGrade_promoteMartinTwoTimes_shouldFail() {
        System.out.println("-----------");
        System.out.println("Message should show:");
        Employee martin = company.getByName("Martin Scavo");
        company.promotePayGrade(martin);
        company.promotePayGrade(martin);
        assertNotEquals(4, martin.getPayGrade());
        System.out.println("-----------");
    }

    @Test
    public void promoteToManager_promoteDerick() {
        Employee derick = company.getByName("Derick Red");
        company.promoteToManager(derick);
        Employee derickP = company.getByName("Derick Red");
        assertTrue(derickP.isManager());
    }

    @Test
    public void addEmployee_addMarkNotManager() {
        Employee derick = company.getByName("Derick Red");
        company.addEmployee("Mark Feler",derick,null,3,true);
        Employee derickP = company.getByName("Derick Red");
        assertEquals("Mark Feler",derickP.getDirectReports().get(0).getName());
    }

    @Test
    public void addEmployee_addMarkNotManager_tooHighPayGrade_shouldFail() {
        System.out.println("-----------");
        System.out.println("Message should show:");
        Employee derick = company.getByName("Derick Red");
        company.addEmployee("Mark Feler",derick,null,7,true);
        Employee mark = company.getByName("Mark Feler");
        assertNotEquals(7,mark.getPayGrade());
        System.out.println("-----------");
    }

    @Test
    public void addEmployee_addMarkManager() {
        Employee brad = company.getByName("Brad Scott");
        Employee martin = company.getByName("Martin Scavo");
        Employee max = company.getByName("Max Payne");
        List<Employee> reports = new ArrayList<>();
        reports.add(max);
        reports.add(martin);
        company.addEmployee("Mark Feler",brad,reports,3,true);
        Employee bradP = company.getByName("Brad Scott");
        Employee mark = company.getByName("Mark Feler");
        assertEquals("Mark Feler",bradP.getDirectReports().get(0).getName());
        assertEquals("Max Payne",mark.getDirectReports().get(0).getName());
    }

    @Test
    public void getBossChain_getMaxBosses() {
        Employee max = company.getByName("Max Payne");
        Employee brad = company.getByName("Brad Scott");
        Employee fred = company.getByName("Fred Jackson");
        Employee john = company.getByName("John Smith");
        List<Employee> expected = new ArrayList<>();
        expected.add(brad);
        expected.add(fred);
        expected.add(john);
        List<Employee> actual = max.getBossChain();
        assertEquals(expected,actual);
    }

    @Test
    public void getDirectReports_getJohnsReports() {
        Employee john = company.getByName("John Smith");
        Employee fred = company.getByName("Fred Jackson");
        Employee jack = company.getByName("Jack Olivo");
        Employee derick = company.getByName("Derick Red");
        List<Employee> expected = new ArrayList<>();
        expected.add(fred);
        expected.add(jack);
        expected.add(derick);
        List<Employee> actual = john.getDirectReports();
        assertEquals(expected,actual);
    }


}
