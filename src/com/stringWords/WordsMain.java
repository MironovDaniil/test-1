package com.stringWords;

import java.util.*;

/**
 * Created by Varamadon on 06.08.2018.
 */
public class WordsMain {

    public static Map<Integer, Integer> wordPerLengthCounter(String input) {
        String correctedInput = input.replaceAll("\\p{Punct}", "");
        String[] words = correctedInput.split("\\s");
        Map<Integer, Integer> result = Arrays.stream(words).map(String::length).collect
                (HashMap::new, (m, c) -> m.put(c, m.containsKey(c) ? (m.get(c) + 1) : 1), HashMap::putAll);
        return result;
    }

    public static void main(String[] args) {
        String text = "1 22 55555, 1 1. 22 22 666'666 333 666666";
        Map<Integer, Integer> result = wordPerLengthCounter(text);
        System.out.println("______");
        result.forEach( (k, v) -> System.out.println(k + " -> " + v));
    }
}
