package com.companyStructure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Varamadon on 08.08.2018.
 */
public class Company {
    private Employee bigBoss;

    private Company(List<Employee> employees, Employee bigBoss) {
        this.bigBoss = bigBoss;
    }

    public Company(String bossName) {
        this(new ArrayList<>(), new Manager(null, 10, true, new ArrayList<>(), bossName));
    }

    public Employee getBigBoss() {
        return bigBoss;
    }

    private Employee getByNameRec(Employee current, String name) {
        if (current.getName().equals(name))
            return current;
        if (!current.isManager()) {
            return null;
        }
        for (Employee e : current.getDirectReports()
        ) {
            Employee promRes = getByNameRec(e, name);
            if (promRes != null) {
                return promRes;
            }
        }
        return null;
    }

    public Employee getByName(String name) {
        Employee result = getByNameRec(bigBoss, name);
        if (result != null) {
            return result;
        } else {
            System.out.println("No employee with such name in this company!");
            return null;
        }
    }

    public void deleteEmployee(Employee toDelete) {
        if (toDelete.isManager()) {
            for (Employee e : toDelete.getDirectReports()
            ) {
                toDelete.getBoss().addDirectReport(e);
            }
        }
        toDelete.getBoss().deleteDirectReport(toDelete);
    }

    public void promotePayGrade(Employee promoted) {
        if (promoted.getPayGrade() == promoted.getBoss().getPayGrade()) {
            System.out.println("Pay grade is too high, promote boss first! Doing nothing");
        } else {
            promoted.setPayGrade(promoted.getPayGrade() + 1);
        }
    }


    public void promoteToManager(Employee promoted) {
        if (promoted.isManager()) {
            System.out.println("Employee is already a manager! Doing nothing");
        } else {
            String name = promoted.getName();
            Employee boss = promoted.getBoss();
            int payGrade = promoted.getPayGrade();
            boolean isTaxResident = promoted.isTaxResident();
            deleteEmployee(promoted);
            addEmployee(name, boss, new ArrayList<>(), payGrade, isTaxResident);
        }
    }

    public void addEmployee(String name,
                            Employee boss,
                            List<Employee> directReports,
                            int payGrade,
                            boolean isTaxResident) {
        Employee correctedBoss;
        String bossName = boss.getName();
        if (!boss.isManager()) {
            promoteToManager(boss);
            correctedBoss = getByName(bossName);
        } else {
            correctedBoss = boss;
        }
        if (directReports == null) {
            Employee added = new Employee(correctedBoss, payGrade, isTaxResident, name);
            correctedBoss.addDirectReport(added);
        } else {
            int maxReportPayGrade = 0;
            int correctedPayGrade = payGrade;
            for (Employee e : directReports
            ) {
                if (e.getPayGrade() > maxReportPayGrade) {
                    maxReportPayGrade = e.getPayGrade();
                }
            }
            if (maxReportPayGrade > correctedBoss.getPayGrade()) {
                System.out.println("Cant add manager with those direct reports:" +
                        " their pay grade is higher than future boss! Doing nothing");
                return;
            }
            if (maxReportPayGrade > payGrade) {
                System.out.println("Added manager have pay grade less then future direct reports:" +
                        " raising added manager's pay grade");
                correctedPayGrade = maxReportPayGrade;
            }
            Employee added = new Manager(correctedBoss, correctedPayGrade, isTaxResident, directReports, name);
            correctedBoss.addDirectReport(added);

        }
    }

}
