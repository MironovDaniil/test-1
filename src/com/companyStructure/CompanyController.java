package com.companyStructure;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CompanyController {
    private Company company;

    public CompanyController() {
        company = new Company("John Smith");
        Employee maxPayne = new Employee(null, 1, true, "Max Payne");
        Employee martinScavo = new Employee(null, 2, false, "Martin Scavo");
        Employee gregHouse = new Employee(null, 3, false, "Greg House");
        List<Employee> brads = new ArrayList<>(2);
        brads.add(maxPayne);
        brads.add(martinScavo);
        List<Employee> jacks = new ArrayList<>(1);
        jacks.add(gregHouse);
        Employee bradScott = new Manager(null, 3, false, brads, "Brad Scott");
        List<Employee> freds = new ArrayList<>(1);
        freds.add(bradScott);
        Employee fredJackson = new Manager(company.getBigBoss(), 9, true, freds, "Fred Jackson");
        Employee jackOlivo = new Manager(company.getBigBoss(), 8, false, jacks, "Jack Olivo");
        Employee derickRed = new Employee(company.getBigBoss(), 4, true, "Derick Red");
    }

    @RequestMapping("/employeeBaseInfo")
    public EmployeeBaseInfo employeeBaseInfo(@RequestParam(value="name") String name) {
        return new EmployeeBaseInfo(company.getByName(name));
    }

    @RequestMapping("/employeeFullInfo")
    public Employee employeeFullInfo(@RequestParam(value="name") String name) {
        if (company.getByName(name)!=null) {
            return company.getByName(name);
        } else {
            return new Employee(null,0,false,"No such employee in this company!");
        }
    }
}
