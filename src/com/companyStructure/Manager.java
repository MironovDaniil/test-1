package com.companyStructure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Varamadon on 08.08.2018.
 */
public class Manager extends Employee {
    private List<Employee> directReports;

    public Manager(Employee boss, int payGrade, boolean isTaxResident, List<Employee> directReports, String name) {
        super(boss, payGrade, isTaxResident, name);
        this.directReports = new ArrayList<>();
        for (Employee e : directReports
        ) {
            this.directReports.add(e);
            if (e.getBoss() != null) {
                e.getBoss().deleteDirectReport(e);
            }
            e.setBoss(this);
        }
    }

    @Override
    public List<Employee> getDirectReports() {
        return directReports;
    }

    @Override
    public boolean isManager() {
        return true;
    }

    @Override
    public void addDirectReport(Employee added) {
        if (directReports.contains(added)) {
            //System.out.println("This employee is already a direct report of that manager! Doing nothing");
            return;
        }
        directReports.add(added);
        added.setBoss(this);
    }

    @Override
    public void deleteDirectReport(Employee deleted) {
        directReports.remove(deleted);
    }
}
