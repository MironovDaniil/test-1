package com.companyStructure;

import java.util.ArrayList;
import java.util.List;

public class EmployeeBaseInfo {
    private String name;
    private String bossName;
    private List<String> directReportsNames;
    private int payGrade;
    private boolean isTaxResident;

    EmployeeBaseInfo (Employee employee) {
        if (employee!=null) {
            this.name = employee.getName();
            if (employee.getBoss()!=null) {
                this.bossName = employee.getBoss().getName();
            } else {
                this.bossName = "Employee is the biggest boss!";
            }
            directReportsNames = new ArrayList<>();
            if (!employee.isManager()) {
                directReportsNames.add("Employee is not a manager!");
            } else {
                for (Employee report:employee.getDirectReports()
                ) {
                    directReportsNames.add(report.getName());
                }
            }
            this.payGrade = employee.getPayGrade();
            this.isTaxResident = employee.isTaxResident();
        } else {
            this.name = "No such employee in this company!";
            this.bossName = "";
            directReportsNames = new ArrayList<>();
            this.payGrade = 0;
            this.isTaxResident = false;
        }
    }

    public String getName() {
        return name;
    }

    public String getBossName() {
        return bossName;
    }

    public List<String> getDirectReportsNames() {
        return directReportsNames;
    }

    public int getPayGrade() {
        return payGrade;
    }

    public boolean isTaxResident() {
        return isTaxResident;
    }
}
