package com.companyStructure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Varamadon on 07.08.2018.
 */
public class Employee {
    private String name;
    private Employee boss;
    private int payGrade;
    private boolean isTaxResident;

    public Employee(Employee boss, int payGrade, boolean isTaxResident, String name) {
        this.name = name;
        this.boss = boss;
        this.payGrade = payGrade;
        this.isTaxResident = isTaxResident;
        if (this.boss != null) {
            if (this.payGrade > boss.getPayGrade()) {
                System.out.println("Pay grade exceed's bosses pay grade, lowering it to bosses pay grade");
                this.payGrade = boss.getPayGrade();
            }
            boss.addDirectReport(this);
        }
    }

    public Employee getBoss() {
        return boss;
    }

    public int getPayGrade() {
        return payGrade;
    }

    public String getName() {
        return name;
    }

    public boolean isTaxResident() {
        return isTaxResident;
    }

    public boolean isManager() {
        return false;
    }

    public List<Employee> getDirectReports() {
        return null;
    }

    public List<Employee> getBossChain() {
        List<Employee> result = new ArrayList<>();
        Employee currentBoss = this.getBoss();
        while (currentBoss != null) {
            result.add(currentBoss);
            currentBoss = currentBoss.getBoss();
        }
        return result;
    }

    public void setBoss(Employee boss) {
        this.boss = boss;
    }

    public void setPayGrade(int payGrade) {
        this.payGrade = payGrade;
    }

    public void addDirectReport(Employee added) {
        System.out.println("Employee is not a manager! Can't add report. Doing nothing");
    }

    public void deleteDirectReport(Employee deleted) {
        System.out.println("Employee is not a manager! Can't delete report. Doing nothing");
    }
}
