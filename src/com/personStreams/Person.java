package com.personStreams;

/**
 * Created by Varamadon on 06.08.2018.
 */
public class Person {
    public enum Gender {Male, Female}

    ;
    private final Gender gender;
    private final double age;

    public Person(Gender gender, double age) {
        this.gender = gender;
        this.age = age;
    }

    public Person() {
        Gender gender;
        double genderDecider = Math.random();
        if (genderDecider < 0.5) {
            gender = Gender.Male;
        } else {
            gender = Gender.Female;
        }
        this.age = 14 + 26 * Math.random();
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    public double getAge() {
        return age;
    }

    public int getFullAge() {
        return (int) age;
    }

}
