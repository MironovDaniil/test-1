package com.personStreams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Varamadon on 07.08.2018.
 */
public class PersonMain {

    public static List<Person> clearFifth (List<Person> input){
        return input.stream().filter((p)->(input.indexOf(p)+1)%5!=0).collect(Collectors.toList());
    }

    public static Map<Integer, Integer> ageCount (List<Person> input){
        return input.stream().map(Person::getFullAge).collect
                (HashMap::new, (m, c) -> m.put(c, m.containsKey(c) ? (m.get(c) + 1) : 1), HashMap::putAll);
    }


    public static void main(String[] args) {
        List<Person> ololo;

    }
}
