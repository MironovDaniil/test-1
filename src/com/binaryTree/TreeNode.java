package com.binaryTree;

import java.util.Comparator;

/**
 * Created by Varamadon on 07.08.2018.
 */
public class TreeNode<E> {
    private E value;
    private TreeNode<E> left;
    private TreeNode<E> right;
    private TreeNode<E> parent;
    private Comparator<E> comparator;

    private TreeNode(E value, TreeNode<E> left, TreeNode<E> right, TreeNode<E> parent, Comparator<E> comparator) {
        this.value = value;
        this.left = left;
        this.right = right;
        this.parent = parent;
        this.comparator = comparator;
    }

    public TreeNode(E value, Comparator<E> comparator) {
        this(value, null, null, null, comparator);
    }

    public void addValue(E value) {
        if (comparator.compare(this.value, value) < 0) {
            if (this.right != null) {
                right.addValue(value);
            } else {
                right = new TreeNode<E>(value, null, null, this, this.comparator);
            }
        } else {
            if (this.left != null) {
                left.addValue(value);
            } else {
                left = new TreeNode<E>(value, null, null, this, this.comparator);
            }
        }
    }

    public boolean isRoot() {
        return parent == null;
    }

    public static int treeHeight(TreeNode node) {
        if (node == null) {
            return 0;
        } else {
            return 1 + Math.max(treeHeight(node.left), treeHeight(node.right));
        }
    }

    public E getValue() {
        return value;
    }

    public TreeNode<E> getLeft() {
        return left;
    }

    public TreeNode<E> getRight() {
        return right;
    }

    public TreeNode<E> getParent() {
        return parent;
    }

    public Comparator<E> getComparator() {
        return comparator;
    }
}
